#!/bin/bash

# Ensure we're root
if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as root" 1>&2
  exit 1
fi

# Download repo
TMP=$(mktemp -d)
curl -sL https://gitlab.com/finwo/fselect/-/archive/stable/fselect-stable.tar.gz | tar -xzf - -C "${TMP}" --strip-components=1

# Build & install
cd "${TMP}"
./configure && make && make install

# Remove downloads
rm -rf "${TMP}"
